# OPB Character Counter

## <https://jwsy.gitlab.io/opb-character-counter/>

<img src="static/opb-character-counter.gif" alt="drawing" height="480"/>

## About

OPB Character Counter is a simple web-based tool designed to help users monitor the character count of the text they input. This app provides a dynamic character counting feature that highlights the text count in red and makes it flash if it exceeds a user-defined maximum limit. The default character limit is set to 350 but can be adjusted between 140 and 1000 according to user preference.

This was made because the MS Word status bar doesn't warn you if you go over a character limit. 

You can turn on a character count in MS word if you right-click on the Status bar and check the Character Count (with spaces) option, but there's no convenient warning.


## Usage

Download the `public` folder and open the `index.html` file locally on your machine, this is pure HTML/CSS/JS plus one mp3 file. 

Optionally, browse to <https://jwsy.gitlab.io/opb-character-counter/> 




## Features

- **Character Counting**: Real-time character counting as you type.
- **Dynamic Limit Adjustment**: Users can set their desired maximum character limit.
- **Visual Alerts**: The character count turns red and flashes when the limit is exceeded.
- **Responsive Design**: Works on various devices and screen sizes.

## Development Notes

This repo is based on the 'Pages/Plain HTML' project template from GitLab. This was written mostly with ChatGPT including this README. 

Special thanks to:
* <https://gitlab.com/seanwasere/fork-me-on-gitlab>
* <https://www.youtube.com/watch?v=FQc5zRy6wBU>